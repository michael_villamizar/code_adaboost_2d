 AdaBoost Algorithm (2D)
 
 Description:
   This program performs the classification of two different classes using 
   the AdaBoost algorithm. In this demo, the samples belong to a feature 
   space of two dimensions (x1,x2) for the sake of simplicity. 

   More precisely, this program computes a two-class classifier, the 
   strong classifier (H), that ensembles -iteratively- a series of weak 
   classifiers (WCs). Each weak classifier is a decision stump that splits
   on one axe the feature space (2D) according to a split threhold (theta)
   and split sign (sig).

 Comments:
   The parameters of the classifier and the 2D feature scenario can be 
   found in the fun_parameters function.

   Two different sets of samples are used in this program. The first one 
   -trnSamples- is used to train the classifier, whereas the second one 
   -tstSamples- is used to evaluate the generalization capabilities of the
   classifier.

   Two different scenarios to train and test the classifier have been
   considered, each one with a particular degree of complexity.

 Steps:
   Steps to exucute the program:
     1. Run the prg_setup.m file to configure the program paths.
     2. Run the prg_adaboost.m file to compute the classifier and perform
        classification over two different classes.

 Contact:
   Michael Villamizar
   mvillami-at-iri.upc.edu
   Institut de Robòtica i Informática Industrial CSIC-UPC
   Barcelona - Spain
   2014

