%% AdaBoost Algorithm (2D)
% 
% Description:
%   This program performs the classification of two different classes using 
%   the AdaBoost algorithm. In this demo, the samples belong to a feature 
%   space of two dimensions (x1,x2) for the sake of simplicity. 
%
%   More precisely, this program computes a two-class classifier, the 
%   strong classifier (H), that ensembles -iteratively- a series of weak 
%   classifiers (WCs). Each weak classifier is a decision stump that splits
%   on one axe the feature space (2D) according to a split threhold (theta)
%   and split sign (sig).
%
% Comments:
%   The parameters of the classifier and the 2D feature scenario can be 
%   found in the fun_parameters function.
%
%   Two different sets of samples are used in this program. The first one 
%   -trnSamples- is used to train the classifier, whereas the second one 
%   -tstSamples- is used to evaluate the generalization capabilities of the
%   classifier.
%
%   Two different scenarios to train and test the classifier has been
%   considered, each one with a particular degree of complexity.
%
% Contact:
%   Michael Villamizar
%   mvillami-at-iri.upc.edu
%   Institut de Robòtica i Informática Industrial CSIC-UPC
%   Barcelona - Spain
%   2014
%

%% main function
function prg_adaboost()
clc,clear all,close all

% message
fun_messages('Adaboost (2D)','presentation');
fun_messages('Adaboost','title');

% delete variables (comment to use previous variables)
fun_delete_variables();

% compute samples
trnSamples = fun_samples('train');  % training samples
tstSamples = fun_samples('test');  % test samples

% show samples
fun_show_samples(trnSamples,'train');  % training samples
fun_show_samples(tstSamples,'test');  % test samples

% compute the classifier with the training data -training samples-
clfr = fun_classifier(trnSamples);

% show classification map
fun_show_classification_map(clfr);

% test the classifier on the training data and test data -generalization-
fun_test(clfr,trnSamples,'train');
fun_test(clfr,tstSamples,'test');

% message
fun_messages('End','title');
end




