%% show samples
% This function shows the samples -positive and negative- in the 2D space
function fun_show_samples(samples,text)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();
fs = prms.visualization.fontSize;  % font size
lw = prms.visualization.lineWidth;  % line width
ms = prms.visualization.markerSize;  % marker size
posColor = prms.visualization.posColor;  % positive class color
negColor = prms.visualization.negColor;  % negative class color

% positive and negative sample indexes
posIndxs = find(samples(:,3)==+1);
negIndxs = find(samples(:,3)==-1);

% figure
figure,plot(samples(posIndxs,2),samples(posIndxs,1),'+','color',posColor,'markersize',ms,'linewidth',lw), hold on
plot(samples(negIndxs,2),samples(negIndxs,1),'o','color',negColor,'markersize',ms,'linewidth',lw), hold on
axis([0,1,0,1]), grid on
title(sprintf('%s Samples',text),'fontsize',fs);
legend('Positives','Negatives');

end
