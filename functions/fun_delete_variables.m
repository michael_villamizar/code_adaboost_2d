%% delete variables
% This function removes the temporal variables.
function fun_delete_variables()

% delete
delete './variables/classifier.mat';
delete './variables/train_samples.mat';
delete './variables/test_samples.mat';
end
