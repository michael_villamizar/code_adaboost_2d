%% test
%  This function test the boosted classifier over the input samples.
function fun_test(clfr,samples,text)
if (nargin~=3), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();
fs = prms.visualization.fontSize;  % font size
lw = prms.visualization.lineWidth;  % line width
ms = prms.visualization.markerSize;  % marker size
posColor = prms.visualization.posColor;  % positive class color
negColor = prms.visualization.negColor;  % negative class color
numSamples = size(samples,1);  % num. samples

% variables
step = 0.001;  % threhold step
maxVal = 0.0;  % max. classification value
labels = samples(:,3);  % sample labels

% positive and negative samples indexes
posIndxs = find(labels==+1);
negIndxs = find(labels==-1);

% num. positive and negative samples
numPosSamples = size(posIndxs,1);
numNegSamples = size(negIndxs,1);

% allocate
scores = size(numSamples,1);  % classification scores

% compute classification scores over the samples
for iterSample = 1:numSamples
    % current sample
    sample = samples(iterSample,1:2);
    % test the classifier
    scores(iterSample,1) = fun_test_classifier(clfr,sample);
end

% threholding
for iterThr = 0:step:1
    
    % classification vector
    vec = 2*(scores>iterThr) - 1;
    
    % true positives, false positives and false negatives
    tmp = vec.*labels;
    tps = sum(tmp(posIndxs)==+1,1);
    fps = sum(tmp(negIndxs)==-1,1);
    fns = numPosSamples - tps; 
    
    % recall and precision rates
    pre = tps/(tps + fps);
    rec = tps/(tps + fns);
    
    % F-measure value
    val = 2*(pre*rec)/(pre+rec);
    
    % best threshold
    if (val>maxVal)
        % update 
        maxVal = val;  % max. classification value
        thr = iterThr;  % threshold
    end
    
end

% messages
fun_messages(sprintf('test: %s samples',text),'process');
fun_messages(sprintf('F-measure: %.3f',maxVal),'information');
fun_messages(sprintf('classification threshold: %.3f',thr),'information');

% figure
figure,axis([0,1,0,1]), grid on, hold on;
title(sprintf('results: %s samples',text),'fontsize',fs);
xlabel(sprintf('F-Measure: %.3f',maxVal),'fontsize',fs);

% show samples
for iterSample = 1:numSamples
    
    % samples properties
    label = samples(iterSample,3);  % label
    score = scores(iterSample);  % score
    sample = samples(iterSample,1:2);  % data -location-
    
    % true positives
    if (label==+1)&&(score>thr)
        plot(sample(2),sample(1),'+','color',posColor,'markersize',ms,'linewidth',lw), hold on;
    end
    
    % false negatives
    if (label==+1)&&(score<=thr)
        plot(sample(2),sample(1),'+','color',[0,0,0],'markersize',ms,'linewidth',lw), hold on;
    end
    
    % true negatives
    if (label==-1)&&(score<=thr)
        plot(sample(2),sample(1),'o','color',negColor,'markersize',ms,'linewidth',lw), hold on;
    end
    
    % true positives
    if (label==-1)&&(score>thr)
        plot(sample(2),sample(1),'o','color',[0,0,0],'markersize',ms,'linewidth',lw), hold on;
    end
    
end

end
