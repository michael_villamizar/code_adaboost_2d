%% classifier
%  This function computes the classifier (H) using AdaBoost where at each 
%  iteration it computes the weak classifier that best discriminates 
%  positive samples from negative samples. 
function output = fun_classifier(samples)
if (nargin~=1), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();  % program parameters
numWCs = prms.classifier.numWCs;  % num. weak classifiers
numSamples = prms.classes.numPosSamples + prms.classes.numNegSamples;  % num. training samples

% message
fun_messages('AdaBoost:','process');
fun_messages(sprintf('num. weak classifiers: %d',numWCs),'information');
fun_messages(sprintf('num. samples: %d',numSamples),'information');

% load/compute samples
try
    
    % load the classifier
    clfr = fun_data_load('./variables/','classifier.mat');
    
    % messages
    fun_messages('the classifier were loaded successfully','information');
    
catch ME
    
    % weights
    weights = ones(numSamples,1)/numSamples;
    
    % boosting iterations
    for t = 1:numWCs
        
        % normalization
        weights = weights./sum(weights,1);
        
        % compute weak classifier (WC)
        WC = fun_weak_learner(weights,samples);
        
        % show weak classifier
        if(t<5), fun_show_weak_classifier(WC,samples,weights,t); end
        
        % beta
        beta = WC.error/(1-WC.error);
        
        % check
        if (beta==1), fun_show_message('perfect weak classifier found','warning'); end
        
        % alpha
        alpha = 0.5*log(1/beta);
        
        % update samples weights
        weights = weights.*exp(-alpha*samples(:,3).*WC.clf);
        
        % assemble the strong classifier (H)
        clfr.WCs(t) = WC;  % weak classifier (WC)
        clfr.numWCs = t;  % num. weak classifiers
        clfr.alphas(t) = alpha;  % alpha weight
        
        % message
        fun_messages(sprintf('weak classifier: %d',t),'information');
        fun_messages(sprintf('  x: %d; theta: %.4f; sig: %d',WC.x,WC.theta,WC.sig),'information');
        fun_messages(sprintf('  alpha: %.3f',alpha),'information');
        
        % pause
        %pause(1)
    end
    
    % save the classifier
    fun_data_save(clfr,'./variables/','classifier.mat');
    
end

% output
output = clfr;
end

%% weak learner
%  This function computes a weak classifier based on the samples weights.
%  Nota: In this example (demo), a weak classifier is defined by a feature 
%  (x1 or x2), a treshold (theta) and a sign (sig). This classifier splits 
%  feature space (2D), and the training samples. The classifier that best 
%  discriminates (separate) positive samples from negative ones, under the
%  current iteration, is chosen for the strong classifier (H). Therefore,
%  the weak learner must seek for the best set of parameters (x,theta,sig)
%  from all possible options.
function output = fun_weak_learner(weights,samples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();  % program parameters
thrStep = prms.classifier.thrStep;  % threshold step

% variables
minErr = 1;  % min. classification error
labels = samples(:,3);  % samples labels

% features : axe x1 or x2
for x = 1:2
    % sign : -1 (<) or +1 (>)
    for sig = -1:2:1
        % feature threshold
        for theta = 0:thrStep:1
            
            % classification vector
            clf = 2*(samples(:,x)*sig>theta*sig) - 1;
            
            % classification error
            err = sum(0.5*abs(clf-labels).*weights,1);
            
            % current best weak classifier
            if (err<minErr && err<0.5)
            
                % update min. error
                minErr = err;
              
                % weak classifier
                WC.x = x;  % feature    
                WC.sig = sig;  % sign
                WC.theta = theta;  % threshold
                WC.clf = clf;  % classification vector
                WC.error = err;  % classification error
                
            end
        end
    end
end

% check
if (minErr==1), fun_messages('Weak Classifier not found','error'); end

% output
output = WC;
end

%% show weak classifier    
function fun_show_weak_classifier(WC,samples,weights,index)    
if (nargin~=4), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();
fs = prms.visualization.fontSize;  % font size
lw = prms.visualization.lineWidth;  % line width
ms = prms.visualization.markerSize;  % marker size
posColor = prms.visualization.posColor;  % positive class color
negColor = prms.visualization.negColor;  % negative class color

% figure
figure,subplot(2,2,[1,3]),axis([0,1,0,1]), grid on, hold on;
xlabel('Samples','fontsize',fs);
title(sprintf('Weak Classifier: %d',index),'fontsize',fs);

% switch line coordinates
switch WC.x
    case 1 % x1
        u = [WC.theta,WC.theta];
        v = [0,1];
    case 2 % x2
        v = [WC.theta,WC.theta];
        u = [0,1];
end

% draw line
line(u,v,'color','g','linewidth',lw), hold on

% switch path coordinates
switch WC.x
    case 1 % x1
        % sign
        switch WC.sig
            case -1
                vv = [0,1,1,0];
                uu = [WC.theta,WC.theta,1,1];
            case 1
                vv = [0,1,1,0];
                uu = [0,0,WC.theta,WC.theta];
        end
    case 2 % x2
        % sign
        switch WC.sig
            case -1
                uu = [0,1,1,0];
                vv = [WC.theta,WC.theta,1,1];
            case 1
                uu = [0,1,1,0];
                vv = [0,0,WC.theta,WC.theta];
        end
end

% patch
patch(uu,vv,0.5*ones(1,3));

% positive and negative sample indexes
posIndxs = find(samples(:,3)==+1);
negIndxs = find(samples(:,3)==-1);

% show samples
plot(samples(posIndxs,2),samples(posIndxs,1),'+','color',posColor,'markersize',ms,'linewidth',lw), hold on
plot(samples(negIndxs,2),samples(negIndxs,1),'o','color',negColor,'markersize',ms,'linewidth',lw), hold on

% positive and negative weights
posWeights = weights(posIndxs);
negWeights = weights(negIndxs);

% weights
subplot(2,2,2),plot(posWeights,'-','color',posColor,'lineWidth',lw), hold on
xlabel('# Samples','fontsize',fs),ylabel('Weight','fontsize',fs);
legend('Positive Samples');
subplot(2,2,4),plot(negWeights,'-','color',negColor,'lineWidth',lw), hold on
xlabel('# Samples','fontsize',fs),ylabel('Weight','fontsize',fs);
legend('Negative Samples');

end
