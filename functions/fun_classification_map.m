%% classification map
%  This function computes the 2D classification map of the computed 
%  classifier.
function output = fun_classification_map(clfr)
if (nargin~=1), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();  % program parameters
pc = prms.visualization.posColor;  % positive color
nc = prms.visualization.negColor;  % negative color
gsz = prms.visualization.gridSize;  % grid size 

% allocate
scrMap = zeros(gsz,gsz);       % score map
clfMap = zeros(gsz,gsz,3);     % classification map

% variables
cy = 0;             % counter
step = 1/(gsz-1);   % grid step

% spatial coordinates
for y = 0:step:1
    
    % update counters
    cx = 0;
    cy = cy + 1;
    
    % spatial coordinate
    for x = 0:step:1
        
        % update counter
        cx = cx + 1;
        
        % test the classifier
        score = fun_test_classifier(clfr,[y,x]);
        
        % score map
        scrMap(gsz-cy+1,cx,1) = score;
        
    end
end
 
% max. and min score values
maxScore = max(scrMap(:));
minScore = min(scrMap(:));

% check if classifier output is not normalized [0,1]
if (maxScore>1 || minScore<0)
    % normalize the score map
    scrMap = scrMap - min(scrMap(:));
    scrMap = scrMap./max(scrMap(:));
end

% spatial coordinates
for y = 1:size(scrMap,1)
    for x = 1:size(scrMap,2)
        
        % classifier score value
        score = scrMap(y,x);
        
        % classification color
        color = pc*score + (1-score)*nc;
        
        % classification map value
        clfMap(y,x,1) = color(1);
        clfMap(y,x,2) = color(2);
        clfMap(y,x,3) = color(3);
        
    end
end

% output
output = clfMap;   % classification map
end

