%% test classifier
%  This function tests the classifier over the input sample. This tests 
%  each weak classifier on the sample.
function output = fun_test_classifier(clfr,sample)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% parameters
score = 0;  % classification score
numWCs = size(clfr.WCs,2);  % num. weak classifiers

% sum. alpha values
alphas = sum(clfr.alphas,2);

% weak classifiers (WCs)
for iterWC = 1:numWCs
    
    % weak classifier properties
    x = clfr.WCs(iterWC).x;  % feature x1 or x2
    sig = clfr.WCs(iterWC).sig;  % feature sign
    theta = clfr.WCs(iterWC).theta;  % threshold
    
    % test weak classifier
    if (sample(1,x)*sig > theta*sig)
        % update the classification score
        score = score + clfr.alphas(iterWC);
    end
end

% normalization
score = score/alphas;

% output
output = score;
end
