%% samples
% This function computes the training and test samples according to the
% scenario example. Samples includes positive and negative samples 
% correponding to a two-class classification problem with different degree 
% of complexity.
function output = fun_samples(text)
if (nargin~=1), fun_messages('incorrect input variables','error'); end

% parameters
prms = fun_parameters();  % program parameters
example = prms.classes.example;  % escenario example
numPosSamples = prms.classes.numPosSamples;  % num. positive samples
numNegSamples = prms.classes.numNegSamples;  % num. negative samples

% messages
fun_messages(sprintf('%s samples',text),'process');
fun_messages(sprintf('scenario example: %d',example),'information');
fun_messages(sprintf('num. positive samples: %d',numPosSamples),'information');
fun_messages(sprintf('num. negative samples: %d',numNegSamples),'information');

% load/compute samples
try
    
    % load samples
    samples = fun_data_load('./variables/',sprintf('%s_samples.mat',text));
    
    % messages
    fun_messages('the samples were loaded successfully','information');
    
catch ME
    
    % compute and save samples
    
    % example set
    switch (example)
        case 1
            % samples - example 1
            samples = fun_example_1(numPosSamples,numNegSamples);
        case 2
            % samples - example 2
            samples = fun_example_2(numPosSamples,numNegSamples);
        otherwise
            fun_messages('incorrect scenario example','error');
    end
    
    % save samples
    fun_data_save(samples,'./variables/',sprintf('%s_samples.mat',text));
end

% messages
fun_messages(sprintf('num. samples: %d',size(samples,1)),'information');

% output
output = samples;
end

%% samples: example 1
% This function computes samples for scenario example 1. This example 
% corresponds to two -unimodal- gaussian distributions.
function output = fun_example_1(numPosSamples,numNegSamples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% labels
posLabels = ones(numPosSamples,1);
negLabels = -1*ones(numNegSamples,1);

% clusters: center, sigma and orientation -distributions-
posMean = [.75 .65];
posSigma = [.11 .11];
posTheta = 40*pi/180;
negMean = [.3 .3];
negSigma = [.11 .11];
negTheta = -30*pi/180;

% random samples coordinates
posData = [randn(1,numPosSamples)*posSigma(1); randn(1,numPosSamples)*posSigma(2)];
negData = [randn(1,numNegSamples)*negSigma(1); randn(1,numNegSamples)*negSigma(2)];

% rotation
posMat = [cos(posTheta), -sin(posTheta); sin(posTheta) cos(posTheta)];
negMat = [cos(negTheta), -sin(negTheta); sin(negTheta) cos(negTheta)];
posData = posMat*posData;
negData = negMat*negData;

% traslation
posData(1,:) = posData(1,:) + posMean(1);
posData(2,:) = posData(2,:) + posMean(2);
negData(1,:) = negData(1,:) + negMean(1);
negData(2,:) = negData(2,:) + negMean(2);

% samples
samples = [posData',posLabels; negData',negLabels];

% random order
indexes = randperm(size(samples,1));
samples = samples(indexes,:);

% output
output = samples;
end

%% samples: example 2
% This function computes samples for scenario example 2. This example 
% contains two class distributions with a non-linear layout. The positive
% class distribution -gaussian- is at the space center. whereas the negative 
% distribution presents a ring layout.
function output = fun_example_2(numPosSamples,numNegSamples)
if (nargin~=2), fun_messages('incorrect input variables','error'); end

% labels
posLabels = ones(numPosSamples,1);
negLabels = -1*ones(numNegSamples,1);

% samples properties: distance and orientation from center
negData = 0.38 + 0.1*sign(randn(1,numNegSamples)).*rand(1,numNegSamples);
posData = 0.0 + 0.25*sign(randn(1,numPosSamples)).*rand(1,numPosSamples);
posTheta = 2*pi*rand(1,numPosSamples);
negTheta = 2*pi*rand(1,numNegSamples);

% coordinates
posX = 0.5 + cos(posTheta).*posData;
posY = 0.5 + sin(posTheta).*posData;
negX = 0.5 + cos(negTheta).*negData;
negY = 0.5 + sin(negTheta).*negData;

% samples
samples = [posX',posY',posLabels;negX',negY',negLabels];

% random order
indexes = randperm(size(samples,1));
samples = samples(indexes,:);

% output
output = samples;
end

