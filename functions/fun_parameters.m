%% parameters
% This functions loads the parameters for AdaBoost.
function output = fun_parameters()

% classes
example = 1;  % escenario example {1,2}
numPosSamples = 200;  % num. positive samples
numNegSamples = 200;  % num. negative samples
classes = struct('example',example,'numPosSamples',numPosSamples,'numNegSamples',numNegSamples);

% classifier
numWCs = 30;  % number of weak classifiers
thrStep = 0.001;  % threhold step
classifier = struct('numWCs',numWCs,'thrStep',thrStep);

% visualization
posColor = [0,1,0];  % positive class color
negColor = [1,0,0];  % negative class color
fontSize = 14;  % font size
gridSize = 100;  % grid map size
lineWidth = 4;  % line width
markerSize = 15;  % marker size
visualization = struct('posColor',posColor,'negColor',negColor,'fontSize',fontSize,'lineWidth',lineWidth,...
    'markerSize',markerSize,'gridSize',gridSize);

% parameters
prms.classes = classes;  % classes
prms.classifier = classifier;  % classifier
prms.visualization = visualization;  % visualization

% output
output = prms;
end
